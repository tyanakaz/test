##################################################################################
#
# config_release.rb
#
# [更新日]
#   - 2014/05/22
#
# [解説]
# 	- Sass & Compass 設定ファイル
#
# [参考URL]
# 	- http://dev.classmethod.jp/ria/html5/web-desiner-corder-scss-config/
#
##################################################################################

Encoding.default_external = "UTF-8"

##################################################################################
# プロジェクトのルート位置を指定。デフォルトは「/」。
# この設定は以下の指定を明記していない場合に影響を与えます。（各項目を明記している場合は使われません）
# http_stylesheets_dir
# http_images_dir
# http_javascripts_dir
# http_fonts_dir
##################################################################################
http_path = "/src/"

##################################################################################
# cssファイルのコンパイル先　・・・　config.rbを置いた位置からの相対指定でCSSを置いているディレクトリを指定
##################################################################################
css_dir = "src/css"

##################################################################################
# sassファイルの配置先　・・・　config.rbを置いた位置からの相対指定でsassを置いているディレクトリを指定
##################################################################################
sass_dir = "src/sass"

##################################################################################
# CSS内で利用する画像の配置先　・・・　config.rbを置いた位置からの相対指定で画像を置いているディレクトリを指定
# ここで指定したディレクトリ位置が下記の指定をした際に先頭に挿入されます。
#  - scss内で、background: image-url(画像のパス); を利用した場合
#  - Sprite画像生成時に利用する、sprite-map(画像のパス); を利用した際
##################################################################################
images_dir = "src/"

##################################################################################
# compassによるsprite 実行時の、結合した画像保存先
##################################################################################
generated_images_dir = "src/img"

##################################################################################
# scss内で background: image-url(); で画像を利用した際、デフォルトでは画像へのパスが相対パスになるが、
# http_images_pathを指定することで、パス記述を任意の場所に変更することが可能。
##################################################################################
http_images_path = "//images.tempostar.net/web-corekago/r01"

##################################################################################
# compassによるspriteで結合作成した画像へのパスは、デフォルトでは相対パスになるが、
# http_generated_images_pathを指定することで、パス記述を任意の場所に変更することが可能。
##################################################################################
http_generated_images_path = "//images.tempostar.net/web-corekago/r01/img"

##################################################################################
# JSの配置先　・・・　プロジェクトルートからの相対指定でJavaScriptを置いているディレクトリを指定
##################################################################################
javascripts_dir = "src/js"

##################################################################################
# コンパイル時にデバッグ用のコメント行番号を挿入[true,false]
##################################################################################
line_comments = false

##################################################################################
# コンパイル時に .sass-cacheディレクトリを作るか？[true,false]
#
# [解説]
# Sassを利用すると.sass-cacheという名前のフォルダ名に
# キャッシュファイルが自動で生成されます
##################################################################################
cache = false

##################################################################################
# background: image-url(); をしたときに自動的にタイムスタンプを付加するか？
# デフォルトでは付加されます。付加したくない場合はnone を設定
##################################################################################
# asset_cache_buster :none

##################################################################################
# コンパイル後のCSSのスタイル指定
# [expanded,nested,compact,compressed]
##################################################################################
#
#	expanded: 通常
#	nested: デフォルトだとこの指定。Sassファイルのネストの深さが引き継がれます。
#	compact: セレクタと属性を1行でまとめる
#	compressed: 可能な限り圧縮
#
##################################################################################
# output_style = :expanded
# output_style = :nested
# output_style = :compact
output_style = :compressed

##################################################################################
# ブラウザでSassをデバッグするためのデバッグコメントを挿入
#
# [解説]
# 各ブラウザにアドオン・拡張機能　をインストールすることで、Sassファイルのデバッグが可能になります。
# FirefoxはアドオンからFirebugと FireSassをインストール
# ChromeはWeb StoreからSASS Inspectorをインストール
##################################################################################
#sass_options = {:debug_info => true}

##################################################################################
# sourcemapを有効にする
##################################################################################
# sass_options = { :sourcemap => true }

##################################################################################
# sprite/画像のハッシュを削除
##################################################################################
module Compass::SassExtensions::Functions::Sprites
  def sprite_url(map)
    verify_map(map, "sprite-url")
    map.generate

    # CSS内、画像名の末尾に?ランダムな数値を付加
    rand = rand(100000000)
    hash = '?' + rand.to_s
    generated_image_url(Sass::Script::String.new(map.name_and_hash+hash))
  end
end

# 出力先パスから /spreite/ を削除することで、/img/ に対してsprite結合後のファイルを設置できるようにする
module Compass::SassExtensions::Sprites::SpriteMethods
  def name_and_hash
    s = "#{path}"
    s.slice!("sprite/")
    # "#{path}.png"
    "#{s}.png"
  end

  def cleanup_old_sprites
    Dir[File.join(::Compass.configuration.generated_images_path, "sprite-#{path}.png")].each do |file|
      log :remove, file
      FileUtils.rm file
      ::Compass.configuration.run_sprite_removed(file)
    end
  end
end

module Compass
  class << SpriteImporter
    def find_all_sprite_map_files(path)
      glob = "sprite-*{#{self::VALID_EXTENSIONS.join(",")}}"
      Dir.glob(File.join(path, "**", glob))
    end
  end
end