#!/bin/sh
cd `dirname $0`
echo '=========================================================================='
echo ''
echo '　　　　　Grunt を実行'
echo ''
echo '=========================================================================='
echo ''
echo '　1. ローカルとDev用のファイルをコンパイルします'
echo '      - コンパイル先 src/js'
echo '      - コンパイル先 src/css'
echo ''
echo '　2. SassとCoffeeの監視が始まります'
echo '      - 監視ディレクトリ src/sass, コンパイル先 src/css'
echo '      - 監視ディレクトリ src/coffee, コンパイル先 src/js'
echo ''
echo '=========================================================================='
ulimit -n 2048 && bundle install && npm install && bundle exec grunt develop
