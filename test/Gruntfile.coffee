###
# Gruntfile.coffee
###

'use strict'

module.exports = (grunt) ->

  # 1. package.jsonに記載されているGruntタスクを読み込む
  require('load-grunt-tasks')(grunt);

  # 2. タスクによって、使用するパスを分岐する
  config = {
    src: 'src/'
    dist: 'deploy/'
  };

  # 3. Grunt 実行処理を記述
  grunt.initConfig

    # 4. で設定したパス設定を取得
    config:config,


    # --------------------------------
    # Compass: compass コンパイル タスク
    # package: grunt-contrib-compass
    # --------------------------------
    compass:
      # 開発
      dev:
        options:
          config: 'config.rb'

      # 本番
      release:
        options:
          config: 'config_release.rb'

    # --------------------------------
    # watch : Watch タスク
    # package: grunt-contrib-watch
    # --------------------------------
    watch:

      # Compass(.scss,.sass)の監視
      compass:
        files: ['<%= config.src %>/main/assets/sass/**/*.{scss,sass}']
        tasks: ['compass:dev']

  # ------------------------------------------------------------------------------
  # 5. タスクのグールプ化設定:
  #    タスクをグループ化することで、コマンドラインから grunt を実行する際に、
  #    まとめて処理を行わせることができるようになります。
  # ------------------------------------------------------------------------------

  # ------------------------------------------------------------------------------
  # 5.1【ローカル用】 コマンドラインで grunt を実行時に 最終的に行う処理を記述
  # ------------------------------------------------------------------------------
  grunt.registerTask 'default', [
    'compass:dev'
    'watch'
  ]

  # ------------------------------------------------------------------------------
  # 5.3【本番用】 コマンドラインで grunt:release を実行時に 最終的に行う処理を記述
  # ------------------------------------------------------------------------------
  grunt.registerTask 'release', [
    'clean:src'
    'clean:dist'
    'copy:external' #コンパイルに必要なリソースを external_js と external_css から js,cssにコピー
    'bower:install'
    'clean:bower'
    'copy:bower'
    'compass:release'
    'coffee:release'
    'jst:release'
    'requirejs:release'
    'copy:release'
    'replace:production'
  ]

  return