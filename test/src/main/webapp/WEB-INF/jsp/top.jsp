<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ja">
<head>
<!-- 文字コードの設定 -->
<meta charset="utf-8">
<!--
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
■■□□□■■□□□□□□□□□□□□■■■■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
■□□□□□■□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□
■□□□□□■□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□
■□□□□□■□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□
■□□■□□■□□□□■■■□□□□□□□□■□□□□□□■■■□■□□□□■■■□□□□□■■□■■□□□□□■■■□□□□□□□□□□□□□■■■■■■□□□□□■■■□□□□
■□□■□□■□□□■□□□■□□□□□□□■□□□□□■□□□■■□□□■□□□■□□□■□□■□□■□□□■□□□■□□□□□□□□□□□□□□■□□□□□□□■□□□■□□□
■□□■□□■□□■□□□□□■□□□□□□■□□□□■□□□□□■□□■□□□□□■□□■□□■□□■□□■□□□□□■□□□□□□□□□□□□□■□□□□□□■□□□□□■□□
■□■□■□■□□■■■■■■■□□□□□□■□□□□■□□□□□□□□■□□□□□■□□■□□■□□■□□■■■■■■■□□□□□□□□□□□□□■□□□□□□■□□□□□■□□
■□■□■□■□□■□□□□□□□□□□□□■□□□□■□□□□□□□□■□□□□□■□□■□□■□□■□□■□□□□□□□□□□□□□□□□□□□■□□□□□□■□□□□□■□□
■□■□■□■□□■□□□□□□□□□□□□■□□□□■□□□□□□□□■□□□□□■□□■□□■□□■□□■□□□□□□□□□□□□□□□□□□□■□□□□□□■□□□□□■□□
□■□□□■□□□■□□□□□■□□□□□□■□□□□■□□□□□■□□■□□□□□■□□■□□■□□■□□■□□□□□■□□□□□□□□□□□□□■□□□■□□■□□□□□■□□
□■□□□■□□□□■□□□□■□□□□□□■□□□□□■□□□□■□□□■□□□■□□□■□□■□□■□□□■□□□□■□□□□□□□□□□□□□■□□□■□□□■□□□■□□□
□■□□□■□□□□□■■■■□□□■■■■■■■■□□□■■■■□□□□□■■■□□□□■■□■■□■■□□□■■■■□□□□□□□□□□□□□□□■■■□□□□□■■■□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□
■□□□□■■■□■■■□□■■■□■□□□□■■■□
■■□□□□■□□□■□□□□■□□■■□□□□■□□
■□■□□□■□□□■□□□□■□□■□■□□□■□□
■□■□□□■□□□■□□□□■□□■□■□□□■□□
■□■□□□■□□□■□□□□■□□■□■□□□■□□
■□□■□□■□□□■■■■■■□□■□□■□□■□□
■□□■□□■□□□■□□□□■□□■□□■□□■□□
■□□■□□■□□□■□□□□■□□■□□■□□■□□
■□□□■□■□□□■□□□□■□□■□□□■□■□□
■□□□■□■□□□■□□□□■□□■□□□■□■□□
■□□□■□■□□□■□□□□■□□■□□□■□■□□
■□□□□■■□□□■□□□□■□□■□□□□■■□□
■■□□□□■□□■■■□□■■■□■■□□□□■□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■■■□□□□
□□■■■■■■■■■■■■□□□□□□□□□□□□□□□□□□□□□□□■■■■■■■■■■□□□□□□■■■■■■■■■■□□□□□□□■■■□□□□
□□□□□□□□□□□□□□□□□□□■■■■■■■■■■■■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□■■■□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□■■■□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■■□□□□□□□■■■□□□□
□■■■■■■■■■■■■■■□□□□□□□□□□□□□□□■□□□□□■■■■■■■■■■■■□□□□□□□□□□□□□■□□□□□□□□■■■□□□□
□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□□■□□□□□□□□□□□□■■□□□□□□□□□■□□□□□
□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□■■□□□□□□□□□□□■■□□□□□□□□□□■□□□□□
□□□□□□□□■□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□□■□□□□□□□□□□□■■■□□□□□□□□□□■□□□□□
□□□□□□□■■□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□□■■□□□□□□□□□□■■□■■□□□□□□□□□■□□□□□
□□□□□□□■□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□□■■□□□□□□□□□□■■□□□■■□□□□□□□□□□□□□□
□□□□□□■■□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□□□□□□■■□□□□□□□□□□■■□□□□□■■□□□□□□□□□□□□□
□□□□□■■□□□□□□□□□□□□■■■■■■■■■■■■□□□□□□□□□□■■■□□□□□□□□□□■■□□□□□□□■■□□□□□□■□□□□□
□□□■■■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■■■■□□□□□□□□□□■■■□□□□□□□□□■■□□□□■■■□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□
-->
<!-- アプリケーションのタイトル -->
<title>YAJIMA APP</title>
<!-- スマートフォンでの表示設定 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- アプリケーションの説明 -->
<meta name="description" content="HNH TECORUS ハンズオンセミナー">
<!-- アプリケーションの制作者 -->
<meta name="author" content="Your Name">
<!-- アプリケーションのアイコン(スマートフォン用）を設置 -->
<link rel="apple-touch-icon" href="webclip.png">
<!-- アプリケーションのアイコン(PC用）を設置 -->
<link rel="shortcut icon" href="favicon.ico">
<!-- Google Web Fonts の読み込み -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
<!-- アプリケーションのスタイル指定 -->
<link rel="stylesheet" href="./css/style.css">
<!-- HTML5の要素を、IE6〜8で利用出来るようにするための、ライブラリ（JavaScript）をインクルード -->
<!--[if lt IE 9]>
  <script src="http://getbootstrap.com/2.3.2/assets/js/html5shiv.js"></script>
<![endif]-->
</head>
<body>
<div class="page">

  <!-- HEADER -->
  <header class="header">
    <h1 class="header__logo">YAJIMA APP</h1>
  </header>
  <!-- /HEADER -->

  <!-- FORM -->
  <form:form modelAttribute="topForm" id="topForm" name="topForm" action="/regist" method="POST" class="form">
    <div class="form__item">
      <input type="text" id="sendTweet" name="sendTweet" class="input" placeholder="Hello! World!">
    </div>
    <div class="form__footer">
      <button type="submit" class="btn btn--post">投稿</button>
    </div>
  </form:form>

  <!-- /FORM -->

  <!-- CONTENTS -->
  <div class="contents">
    <div class="content">
      <div class="box">
        <h2 class="box__header"><a href="https://twitter.com/${topForm.screenName}" target="_blank">Tweet</a></h2>
        <div class="box__body">
          <ul class="list">
			<c:forEach var="item" items="${topForm.tweetList}" varStatus="status">
            <li class="list__item">
              <div class="tweet">
                <p class="tweet__avater"><img src="./img/profile.png" width="60" height="60" alt="NHN TECHORUS."></p>
                <p class="tweet__date"><fmt:formatDate value="${item.createdAt}" pattern="yyyy年MM月dd日 HH時mm分ss秒" /></p>
                <div class="tweet__txt">
                  ${item.text}
                </div>
              </div>
            </li>
			</c:forEach>

          </ul>
        </div>
      </div>
    </div>

    <div class="content">
      <div class="box js-articles">
        <h2 class="box__header">Comments</h2>
        <div class="box__body">
          <ul class="list">
			<c:forEach var="item" items="${topForm.myfeedList}" varStatus="status">
	            <li class="list__item js-article" data-id="${item.id}">
	              <div class="article">
	                <p class="article__date"><fmt:formatDate value="${item.createdat}" pattern="yyyy年MM月dd日 HH時mm分ss秒" /></p>
	                <div class="article__txt js-text">${item.text}</div>
	                <div class="article__btn">
	                  <span class="btn btn--edit js-modal__btn" data-type="edit">編集</span>
	                  <span class="btn btn--delete js-modal__btn" data-type="delete">削除</span>
	                </div>
	              </div>
	            </li>
			</c:forEach>

          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- /CONTENTS -->

  <!-- FOOTER -->
  <footer class="footer">
    <small class="footer__copy">Copyright &copy; NHN Techorus Corp. All rights reserved.</small>
  </footer>
  <!-- /FOOTER -->

  <!-- MODAL LAYER -->
  <div class="modalbg is-hide js-modal__bg js-cancel"></div>

  <!-- EDIT MODAL -->
  <form method="put" action="/api/update" class="modal is-hide js-modal__edit">
    <div class="modal__heaer">
      編集
      <div class="modal__close">
        <p class="close js-cancel"></p>
      </div>
    </div>
    <div class="modal__body">
      <div class="modal__item">
        <input type="text" name="tweet" class="input js-input__edit" value="">
      </div>
    </div>
    <div class="modal__footer">
      <span class="btn btn--cancel js-cancel">キャンセル</span>
      <span class="btn btn--update js-update">更新</span>
    </div>
  </form>
  <!-- /EDIT MODAL -->

  <!-- DELETE MODAL -->
  <form method="delete" action="/api/delete" class="modal is-hide js-modal__delete">
    <div class="modal__heaer">
      削除
      <div class="modal__close">
        <p class="close js-cancel"></p>
      </div>
    </div>
    <div class="modal__body">
      <div class="modal__item">
        削除した記事は元に戻すことができません。本当に削除しますか？
      </div>
    </div>
    <div class="modal__footer">
      <span class="btn btn--cancel js-cancel">キャンセル</span>
      <span class="btn btn--delete js-delete">削除</span>
    </div>
  </form>
  <!-- /DELETE MODAL -->

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="./js/app.js"></script>
</body>
</html>