/**
 * app.js
 */
(function($){
  $(function(){

    // bind ui elements
    var $ui = {
      modal__bg : $('.js-modal__bg'),
      modal__btn : $('.js-modal__btn'),
      modal__edit : $('.js-modal__edit'),
      modal__delete : $('.js-modal__delete'),
      input__edit : $('.js-input__edit'),
      cancel : $('.js-cancel'),
      update : $('.js-update'),
//      delete : $('.js-delete')
    };

    // events
    $ui.modal__btn.on({
      "click":function(e){
        e.preventDefault();
        showModal({
          type:$(this).attr('data-type'),
          id:$(this).closest('.js-article').attr('data-id')
        });
      }
    });

    $ui.cancel.on({
      "click":function(){
        hideModal({
          type:$('.js-modal__bg').attr('data-type')
        });
      }
    });

    $ui.update.on({
      "click":function(e){
        e.preventDefault();
        var $target = $(this).closest('.js-modal__edit');
        updateArticle({
          url:$target.attr('action'),
          id:$target.attr('data-id'),
          text:$target.find('.js-input__edit').val()
        });
      }
    });

//    $ui.delete.on({
//      "click":function(e){
//        e.preventDefault();
//        var $target = $(this).closest('.js-modal__delete');
//        deleteArticle({
//          url:$target.attr('action'),
//          id:$target.attr('data-id'),
//        });
//      }
//    });


    /**
     * [showModalBg モーダル背景表示]
     */
    function showModalBg(props){
      var animate = 'animated fadeIn';
      $ui.modal__bg
        .attr('data-type',props.type)
        .removeClass('is-hide')
        .addClass(animate)
        .on({
          'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend':function(){
            $ui.modal__bg
              .off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend')
              .removeClass(animate);
          }
        });
    }

    /**
     * [hideModalBg モーダル背景表示]
     */
    function hideModalBg(){
      var animate = 'animated fadeOut';
      $ui.modal__bg
        .removeAttr('data-type')
        .addClass(animate)
        .on({
          'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend':function(){
            $ui.modal__bg
              .off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend')
              .removeClass(animate)
              .addClass('is-hide');
          }
        });
    }

    /**
     * [showModal モーダル表示処理]
     */
    function showModal(props){
      var $modal;
      var animate = 'animated bounceInUp';
      switch (props.type){
        case "edit":
          $modal = $ui.modal__edit;
          initEditModal(props);
          break;
        case "delete":
          $modal = $ui.modal__delete;
          break;
      }
      showModalBg(props);
      $modal
        .removeClass('is-hide')
        .attr('data-id',props.id)
        .addClass(animate)
        .on({
          'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend':function(){
            $modal
              .off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend')
              .removeClass(animate);
          }
        });
    }

    /**
     * [initEditModal 編集モーダル]
     */
    function initEditModal(props){
      $.ajax({
        type: "GET",
        url: '/api/get',
        dataType: "json",
        data:{
          id:props.id
        },
        cache: false,
        async: false,
        timeout: 5000
      }).done(
        function(res){
          if (res.status === 'Success'){
            $ui.input__edit.val(res.text);
          }
        }
      );
    }

    /**
     * [update 更新]
     */
    function updateArticle(props){
      $.ajax({
        type: "GET",
        url: props.url,
        dataType: 'json',
        data:{
          id:props.id,
          text:props.text
        },
        cache: false,
        async: false,
        timeout: 5000,
      }).done(
        function(res){
          $('.js-articles')
            .find('[data-id="'+ res.id +'"]')
            .find('.js-text')
            .text(res.text);
          hideModal({type:'edit'});
        }
      );
    }

    /**
     * [delete 削除]
     */
    function deleteArticle(props){
      $.ajax({
        type: "GET",
        url: props.url,
        dataType: 'json',
        data:{
          id:props.id
        },
        cache: false,
        async: false,
        timeout: 5000,
      }).done(
        function(res){
          if (res.status === 'Success'){
            $('.js-articles')
              .find('[data-id="'+ res.id +'"]')
              .remove();
            hideModal({type:'delete'});
          }
        }
      );
    }


    /**
     * [hideModal モーダル非表示処理]
     */
    function hideModal(props){
      var $modal;
      var animate = 'animated fadeOutDown';
      switch (props.type){
        case "edit":
          $modal = $ui.modal__edit;
          break;
        case "delete":
          $modal = $ui.modal__delete;
          break;
      }
      $modal
        .removeAttr('data-id')
        .addClass(animate)
        .on({
          'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend':function(){
            $modal
              .off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend')
              .removeClass(animate)
              .addClass('is-hide');
          }
        });
      hideModalBg();
    }

    console.log('%cWelcome to NHN テコラス!',"color: red; font-size: 24px; font-weight: bold;");
    console.log('本日はハンズオンセミナーにご参加いただきありがとうございます!');
    console.log('わからないことがありましたら気軽に我々講師に声をかけてくださいね');
    console.log('');
    console.log('%c# 講師紹介','font-weight: bold; font-size: 16px;');
    console.log('矢島信二 -> yajima()');
    console.log('喜多繁晃 -> kita()');
    console.log('崔玉澤 -> cui()');
    console.log('');
    console.log('%c# 会社紹介','font-weight: bold; font-size: 16px;');
    console.log('会社紹介 -> about()');
    console.log('採用情報 -> joinUs()');
    console.log('');
  });

})(jQuery);

function yajima(){
  console.log('## 矢島信二');
  console.log('1982年 生まれ、34歳。山梨県育ち。趣味は登山。');
  console.log('「わからないことがあったらなんでも聞いてください！」');
  return '';
}

function kita(){
  console.log('## 喜多繁晃');
  console.log('1982年 生まれ、34歳。鎌倉在住。趣味はサーフィン。');
  console.log('「良い時間になるよう全力でサポートします。」');
  return '';
}

function cui(){
  console.log('## 崔玉澤');
  console.log('1982年 生まれ、34歳。東京生まれ。趣味は読書。');
  console.log('「今日はよろしくお願いします。」');
  return '';
}

function about(){
  console.log('会社情報のページに遷移します。');
  setTimeout(
    function() {
      location.href = 'https://nhn-techorus.com/company/profile.html';
    },500
  );
  return '';
}

function joinUs(){
  console.log('採用情報のページに遷移します。');
  setTimeout(
    function() {
      location.href = 'https://nhn-techorus.com/recruit/career/lp/';
    },500
  );
  return '';
}