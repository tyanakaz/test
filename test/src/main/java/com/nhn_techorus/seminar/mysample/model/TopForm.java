package com.nhn_techorus.seminar.mysample.model;

import java.util.List;

import twitter4j.ResponseList;
import twitter4j.Status;

public class TopForm {
	private String sendTweet;
	private String screenName;
	private ResponseList<Status> tweetList;
	private List<Myfeed> myfeedList;

	public String getSendTweet() {
		return sendTweet;
	}
	public void setSendTweet(String sendTweet) {
		this.sendTweet = sendTweet;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public ResponseList<Status> getTweetList() {
		return tweetList;
	}
	public void setTweetList(ResponseList<Status> tweetList) {
		this.tweetList = tweetList;
	}
	public List<Myfeed> getMyfeedList() {
		return myfeedList;
	}
	public void setMyfeedList(List<Myfeed> myfeedList) {
		this.myfeedList = myfeedList;
	}

}
