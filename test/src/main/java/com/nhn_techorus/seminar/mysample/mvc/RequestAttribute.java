package com.nhn_techorus.seminar.mysample.mvc;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestAttribute {
	public String value() default "";
	public String name() default "";
	public Class<? extends Annotation> requiredAnnotation() default NoAnnotationRequired.class;
	public String[] urlPatterns() default {};
	public static @interface NoAnnotationRequired {
	}

}
