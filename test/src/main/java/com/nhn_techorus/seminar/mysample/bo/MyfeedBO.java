package com.nhn_techorus.seminar.mysample.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;

import com.nhn_techorus.seminar.mysample.dao.MyfeedDAO;
import com.nhn_techorus.seminar.mysample.model.Myfeed;

@Service
public class MyfeedBO {

	private String CONSUMER_KEY = "ctMnSslQCSD4FwExxdjirOXv3";
	private String CONSUMER_SECRET = "F40FtxsGaYCllCYS7yJ4e2uAXz2BehBFqO2d3lrxlAkT7hinKf";
	private String ACCESS_TOKEN = "4859520857-BKQJHuHPKqW4YOKUjioGV1Qp0MmpLYZeWCQcdxq";
	private String ACCESS_TOKEN_SECRET = "YYrfhoT5gGuVP7h7Osbab1HSgY0y7mWQfUGuTEI4Lru6C";

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MyfeedDAO myfeedDAO;

	private Twitter twitter = TwitterFactory.getSingleton();

	/**
	 * Twitter Timelineを返却
	 */
	public ResponseList<Status> getTimeline() throws Exception {
		ResponseList<Status> list = null;
		try {
			twitterAuthentication();
			list = twitter.getHomeTimeline();
		} catch (Exception e) {
			log.info(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * Twitter認証ユーザ名を返却
	 */
	public String getScreenName() throws Exception {
		String name = null;
		try {
			twitterAuthentication();
			name = twitter.getScreenName();
		} catch (Exception e) {
			log.info(e.getMessage(), e);
		}
		return name;
	}

	//https://twitter.com/shinji_yajima/status/700672680760995842

	/**
	 * Myfeedテーブルのリストを返却
	 * @return
	 */
	public List<Myfeed> getMyfeedList() {
		return myfeedDAO.select();
	}

	/**
	 * Myfeedテーブルのリストを返却
	 * @return
	 */
	public Myfeed getMyfeed(int id) {
		return myfeedDAO.selectForId(id);
	}

	/**
	 * 引数textをmyfeedテーブルにinsertし、
	 * Twitter APIを利用して、つぶやき(text)をツイート。
	 * no transaction
	 *
	 * @param text つぶやき内容
	 * @throws Exception
	 */
	public void tweetAndInsert(final String text) throws Exception {
		myfeedDAO.insert(text);
		try {
			twitterAuthentication();
			twitter.updateStatus(text);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 引数idをキーとするMyfeedレコードのtextを引数textの情報に更新
	 *
	 * @param id
	 * @param text
	 */
	public void updateMyfeed(Myfeed myfeed) {
		myfeedDAO.update(myfeed);
	}

	/**
	 * 引数idをキーとするMyfeedレコードを削除
	 * @param id
	 */
	public void deleteMyfeed(int id) {
		myfeedDAO.delete(id);
	}

	/**
	 * twitter認証
	 */
	private void twitterAuthentication() {
		Twitter twitter = TwitterFactory.getSingleton();
		try {
			User user = twitter.verifyCredentials();
		} catch (Exception e) {
			log.info("未認証");
			try {
				twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
				log.info("認証完了");
			} catch (Exception e2) {
				log.info("認証失敗 --- " + e2.getMessage());
			}
		}
		try {
			AccessToken accessToken = new AccessToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
			twitter.setOAuthAccessToken(accessToken);
		} catch(Exception e) {
			log.info("Twitter処理しません。 --- ");
		}

	}


}
