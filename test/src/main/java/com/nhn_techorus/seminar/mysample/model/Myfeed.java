package com.nhn_techorus.seminar.mysample.model;

import java.util.Date;

public class Myfeed {
	private int id;
	private String text;
	private Date createdat;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedat() {
		return createdat;
	}
	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

}
