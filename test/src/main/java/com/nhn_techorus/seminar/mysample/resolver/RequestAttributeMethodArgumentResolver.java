package com.nhn_techorus.seminar.mysample.resolver;

import org.springframework.core.Conventions;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.nhn_techorus.seminar.mysample.mvc.RequestAttribute;


public class RequestAttributeMethodArgumentResolver implements HandlerMethodArgumentResolver {

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		RequestAttribute anno = parameter.getParameterAnnotation(RequestAttribute.class);
		return anno != null;
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {

		RequestAttribute anno = parameter.getParameterAnnotation(RequestAttribute.class);

		String name = "".equals(anno.value()) ? anno.name() : anno.value();
		if ("".equals(name)) {
			name = Conventions.getVariableNameForParameter(parameter);
		}
		return webRequest.getAttribute(name, NativeWebRequest.SCOPE_REQUEST);
	}
}
