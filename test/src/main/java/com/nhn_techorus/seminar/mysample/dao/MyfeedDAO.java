package com.nhn_techorus.seminar.mysample.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.nhn_techorus.seminar.mysample.model.Myfeed;

@Component
public class MyfeedDAO {

    private static final String NAMESPACE = MyfeedDAO.class.getPackage().getName() + ".MyfeedDAO.";

    @Autowired
    @Qualifier("sqlSessionTemplate")
    private SqlSessionTemplate sqlSessionTemplate;

	public void insert(String param) {
		sqlSessionTemplate.insert(NAMESPACE + "insertMyfeed", param);
	}

	public void update(Myfeed param) {
		sqlSessionTemplate.update(NAMESPACE + "updateMyfeed", param);
	}

	public void delete(int param) {
		sqlSessionTemplate.delete(NAMESPACE + "deleteMyfeed", param);
	}

	public List<Myfeed> select() {
		return sqlSessionTemplate.selectList(NAMESPACE + "selectMyfeed");
	}

	public Myfeed selectForId(int param) {
		return sqlSessionTemplate.selectOne(NAMESPACE + "selectMyfeedForId", param);
	}

}
