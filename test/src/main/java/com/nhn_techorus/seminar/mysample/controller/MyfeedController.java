package com.nhn_techorus.seminar.mysample.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nhn_techorus.seminar.mysample.bo.MyfeedBO;
import com.nhn_techorus.seminar.mysample.model.Myfeed;
import com.nhn_techorus.seminar.mysample.model.TopForm;


@Controller
public class MyfeedController {
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MyfeedBO myfeedBO;

	@RequestMapping({"/r_form", "/"})
	public String r_form(HttpServletRequest req, Model model) {
		Myfeed test = new Myfeed();
		test.setId(1);
		test.setText("aaa");
		myfeedBO.updateMyfeed(test);
		return "r_form";
	}

	@RequestMapping({"/top", "/"})
	public String top(HttpServletRequest req, Model model) {
		try {
			TopForm topForm = new TopForm();

			topForm.setScreenName(myfeedBO.getScreenName());
			topForm.setTweetList(myfeedBO.getTimeline());
			topForm.setMyfeedList(myfeedBO.getMyfeedList());

			model.addAttribute("topForm", topForm);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "top";
	}

	@RequestMapping("/regist")
	public String regist(HttpServletRequest request, @ModelAttribute("topForm") TopForm topForm, BindingResult result, Model model) {
		try {
			myfeedBO.tweetAndInsert(topForm.getSendTweet());

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "redirect:/top";
	}

	@RequestMapping("/api/update")
	public @ResponseBody Map<String, Object> update(HttpServletRequest request, @ModelAttribute("myfeed") Myfeed myfeed, BindingResult result, Model model) {
		try {
			myfeedBO.updateMyfeed(myfeed);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ajaxResultSuccess(myfeed.getId(), myfeed.getText());
	}

	@RequestMapping("/api/delete")
	public @ResponseBody Map<String, Object> delete(HttpServletRequest request, @ModelAttribute("myfeed") Myfeed myfeed, BindingResult result, Model model) {
		try {
			myfeedBO.deleteMyfeed(myfeed.getId());

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ajaxResultSuccess(myfeed.getId(), null);
	}

	@RequestMapping("/api/get")
	public @ResponseBody Map<String, Object> get(@ModelAttribute("myfeed") Myfeed myfeed, BindingResult result, Model model) {
		try {
			myfeed = myfeedBO.getMyfeed(myfeed.getId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ajaxResultSuccess(myfeed.getId(), myfeed.getText());
	}

	/**
	 * Ajax APIレスポンスオブジェクト生成用
	 */
	private Map<String, Object> ajaxResultSuccess(int id, String text) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("id", id);
		if (text != null) {
			result.put("text", text);
		}
		result.put("status", "Success");
		return result;
	}

}
