#!/bin/sh
cd `dirname $0`
echo '=========================================================================='
echo ''
echo '　　　　　Grunt の実行に必要な NodePackage をインストールします'
echo ''
echo '=========================================================================='
echo ''
echo '　1. Node.js がインストールされていない場合、エラーになります。'
echo '　2. Ruby がインストールされていない場合、エラーになります。'
echo '　3. node_modules というディレクトリが作成され、'
echo '　   node_modules内にNodePackageがインストールされます。'
echo '　3. node_modules はコミットしないでください。'
echo ''
echo '=========================================================================='
command -v bundle >/dev/null 2>&1 || sudo gem install bundler
command -v grunt >/dev/null 2>&1 || sudo npm install -g grunt-cli
command -v bower >/dev/null 2>&1 || sudo npm install -g bower
npm install